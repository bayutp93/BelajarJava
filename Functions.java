public class Functions{
	public static void main (String[]args){
		/*
		Function 
		grouping the code and reuse it
		
		#structure dari function pada java adalah sbb:
		public void namefunction()
		public = access modifier
		void   = return type
		namefunction = nama dari function
		#pemanggilan function
		namafunction(); 
		contoh :*/
		//Functions function = new Functions();
		//function.playMusic();
		// berhubung method playMusic berstatus static maka kita tidak perlu bikin objek
		playMusic();		
		// kalau fungsinya tidak static maka kita wajib bikin object dulu, contohnya kek gini
		Functions obj = new Functions();
		//method dapat kita gunakan berkali kali sesuka kita(reuse it), contohnya kek gini
		obj.cetakUndangan();
		//obj.cetakUndangan();
		//bayangkan kalau kita tidak menggunakan method bisa berapa baris itu kita tulis code berulang ulang
		//itulah salah satu untungnya kita pake method
		/*
		tentang parameter dan argument
		apa itu parameter? apa itu argument?
		parameter adl variable yang ada di dalam kurung dan dapat digunakan didalam suatu method
		sedangkan argument adalah value yang ditentukan ketika kita memanggil method tersebut
		 */
		int suhu = 5;
		wheater(suhu); //recomended use it
		wheater(2); // suhu dan 2 ini yang disebut dengan argument yang mana adalah value dari parameter temp 
		//so, harus bertipe int knp? karena variable atau parameter temp pada method wheater tipe datanya adalah int
		/*
		multiple paramaters
		kita bisa pake banyak parameter di dalam method kita, dengan dipisahkan oleh tanda komah
		rule penulisannya kek gini
		[access modifier] [return type][nama method](parameter1,parameter2,parameter3,....,paramaterN)
		 */
		int panjang = 10;
		int lebar = 5;
		hitungLuas(panjang,lebar); //yang perlu diingat dalam multiple paramater adalah urutan dari paramaternya tidak boleh dibolak balik ya guys
		//misalnya dalam method kan urutannya variable p kemudian variable l, nah pas di argument nya juga harus gitu guys panjang baru lebar.
		int jmlLike = 0;
		String komentar = "Bagus";
		boolean status = true;
		likePhoto(jmlLike, komentar, status);
		/*
		return value
		jika tadi kita belajar tentang paramater, kita juga udah belajar tentang return sebenarnya yakni return bertupe void,
		void itu tidak ada kembaliannya / tidak ada returnnya.
		jadi gini, paramater merupakan inputan nilai, sedangkan return value itu merupakan output,outputnya mau apa tipe datanya apa
		jika tadi kita hanya mencetak output, outputnya itu ya didalam method tsb berbeda jika kita inginkan outpunya adalah diluar dari method tsb.
		jadi jika tidak ada return (void) maka semua operasi akan selesai di method tersebut, sehingga ketiika kita mau pake kita tinggal pake aja, 
		tidak bisa menambahkan operasi diluar dari method tsb, jika ketika kita ingin pake operasi di luar method maka yang perlu kita lakukan adl
		menggunakan return value.
		yang kita butuhkan ketika ingin menggunakan return value
		1. type return value : int, String, double etc
		2. return statement : return function definition
		contoh :
		 */
		int jumlahLikes = likePhotos(0,"Nice", false);
		int totalLikes = likePhotos(jumlahLikes,"Just great", true);

	}
	public static boolean playButton = true;
	public static void playMusic(){
		if (playButton) {
			System.out.println("Play Music");
		}else{
			System.out.println("Music Stopped");
		}
	}
	public void cetakUndangan(){
		System.out.println("Gue undang lo di pernikahan gue sama doi, jngan lupa datang gan");
		System.out.println("Gue undang lo di pernikahan gue sama doi, jngan lupa datang gan");
		System.out.println("Gue undang lo di pernikahan gue sama doi, jngan lupa datang gan");
	}
	//contoh method pake paramater
	public static void wheater(int temp) {
		if (temp > 30) {
			System.out.println("Suhu diluar sekarang sangat panas");
		}else if (temp < 5) {
			System.out.println("Suhu sekarang sangat dingin");
		}else{
			System.out.println("Suhu sekarang biasa saja tidak panas dan tidak terlalu dingin");
		}
	}
	//contoh method pake multiple paramater
	public static void hitungLuas(int p, int l){
		int luas = p * l;
		System.out.println("Hasil luas persegi panjang = "+luas+" cm");
	}
	//contoh lain multiple paramater
	public static void likePhoto(int currentLikes, String comment, boolean like){
		//feedback comment
		System.out.println("Feedback : "+comment);
		if (like) {
			//jika like true maka jumlah like (currentLike) + 1
			currentLikes = currentLikes+1;
		}
		System.out.println("likes : "+currentLikes);
	}
	//contoh return value
	public static int likePhotos(int curentLikes, String comments, boolean likes){
		System.out.print("feedback "+comments+" ");
		if (likes) {
			curentLikes = curentLikes + 1;
		}
		System.out.println("Likes "+curentLikes);
		return curentLikes;
	}

}