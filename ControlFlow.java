public class ControlFlow{
	public static void main(String[]args){
		/*kita akan mulai belajar if

		if digunakan untuk menghadapi sebuah kondisi, contoh : dalam kehidupan sehari -hari Jika hari ini hujan
		maka kami membutuhkan payung, didalam java juga demikian*/
		boolean hariIniHujan = false;

		if (hariIniHujan) {
			System.out.println("Kami membutuhkan payung");
		}else{
			System.out.println("kami tidak membutuhkan payung");
		}
		// contoh lain adalah traffic light
		boolean isGreenLight = true;
		if (isGreenLight) {
			System.out.println("Drive");
		}
		/*
		Bicara tentang variable scope
		variable scope adl lingkungan suatu variable, sebenarnya ada bebebrapa tipe variable
		ada instance, local kita akan bahas nanti.
		 */
		boolean isRedLight = true; //cangkupan variable ini adl dari baris 24 sampai 30
		if (isRedLight) {
			int speedCar = 60; //sedangkan cangkupan variable ini adalah dari buka kurawal sampai tutup kurawal, jika kita gunakan diluar itu mk akan terjadi error 
			System.out.println("Drive");
			System.out.println("Speed is "+speedCar);
		}
		//speedCar = speedCar - 10; contohnya adalah seperti ini, jika ini tetap dijalankan maka akan terjadi error krn diluar cangkupan dari variale speedCar
		/*
		belajar tentang if else
		else digunakan untuk alternatif lain jika pada kondisi if tidak memenuhi
		contoh */
		boolean merah = false;
		// karena merah bernilai false maka scope if akan di lewati dan komputer akan mengeksekusi scope else
		if (merah) {
			System.out.println("boleh jalan"); //dilewati
		}else{
			System.out.println("harus berhenti"); //dieksekusi
		}
		/*if else
		jika else tanpa menambahkan kondisi lain, sedangkan if else dapat menambahkan kondisi variable lain
		contoh*/
		boolean menang = false;
		boolean seri = false;

		if (menang) { //menang = false maka dilewati
			System.out.println("mendapatkan 3 point");
		} else if (seri) { // jk true maka akan dieksekusi
			System.out.println("mendapatkan 1 point");
		}else{ // jika dua statement if diatas false semua maka akan dieksekusi, jika tidak false ya tidak dieksekusi
			System.out.println("tidak mendapatkan point");
		}

		/*boolean expression
		boolean hanya memiliki value true atau false, namun kita juga bisa menggunakan expression
		misalnya
		*/
		boolean x = 3 > 4; // ini yang dinamakan expression yang mana sama ketika kita menuliskan boolean x = false
		// kamu bisa pake expression lain misal < atau ==, !=, >= , <=
		System.out.println(x); // outputnya : false

		int ticketPrice = 10;

		int age = 30;
		boolean isStrudent = false;

		if (age <= 15) {
			// jika umur mereka kurang dari atau sama dengan 15
			ticketPrice = ticketPrice - 5;
		} else if(age > 60){
			//jika umur mereka lebih dari 60
			ticketPrice = ticketPrice - 5;
		} else if(isStrudent){
			// jika mereka adl pelajar
			ticketPrice = ticketPrice - 5;
		}
		System.out.println("ticket price $"+ticketPrice);

		/*Logika operator
		yang sering digunakan adalah 
		1. AND (&&)
		2. OR (||)
		3. NOT(!)
		AND dan OR digunakan ketika kita membandingkan 2 expression (boolean) sedangkan NOT cuman satu expression
		contoh*/

		boolean a = 3 > 2 && 1 < 2; // akan bernilai true jika kedua expression bernilai true
		boolean b = 3 > 1 || 2 < 1; // akan bernilai true jika salah satu expression bernilai true
		boolean c = !b;
		System.out.println(a); //output : true
		System.out.println(b); //output : true
		System.out.println(c); //output : false

		/*nested if
		nested if atau if bersarang merupakan ketika kita menginginkan ada kondisi lain di dalam if kita
		contoh ada 3 orang yang suka suatu film
		orang pertama suka film action, orang kedua suka film romance dan orang ketiga suka film comedy dan horor
		gimana caranya agar mereka bisa nonton semua
		if (action && romance && (comedy || horror))
		untuk kasus diatas maka kita bisa gunakan nested if, pake cara yang diatas bisa tp complicated krn terlalu panjang*/
		boolean action = true;
		boolean romance = true;
		boolean comedy = false;
		boolean horror = true;
		if (action && romance) {//jika ini true
			System.out.println("nonton film action dan romance disini");
			if (comedy || horror) {// ini juga true maka mereka semua bisa nonton bareng
				System.out.println("nonton film comedy atau horor disini");
			}
		}//outputnya nonton film action dan romance disini dan nonton film comedy ...
		//contoh lain
		int rating = 4;
		if (rating >= 0 && rating <= 5) {
			if (rating <= 2) {
				System.out.println("apa alasanmu memberikan rating rendah?");
			}
			System.out.println("Terimakasih atas feedbacknya :)");
		}
		/*
		membahas tentang switch
		switch ini hampir sama dengan else if,
		fungsi switch ini mencocokkan value dari variable dan mencari value yang sama
		fungsi ini juga bisa menyederhanakan ketika kita gunakan if else if
		contoh */
		int passcode = 434;
		String coffeeType;
		switch(passcode){
			case 444: coffeeType ="latte";
			break;
			case 434: coffeeType = "expresso";
			break;
			case 212: coffeeType = "white";
			break;
			case 111: coffeeType = "black";
			break;
			default: coffeeType = "Unknown";
			break;
		}
		System.out.println(coffeeType);
		//contoh lain
		int mont = 37;
		String montString;
		switch(mont){
			case 1: montString="Januari";
			break;
			case 2: montString="Februari";
			break;			
			case 3: montString="Maret";
			break;
			case 4: montString="April";
			break;
			case 5: montString="Mei";
			break;
			case 6: montString="Juni";
			break;
			case 7: montString="Juli";
			break;
			case 8: montString="Agustus";
			break;
			case 9: montString="September";
			break;
			case 10: montString="Oktober";
			break;
			case 11: montString="November";
			break;
			case 12: montString="Desember";
			break;
			default : montString = "Invalid Mont";
			break;
		}
		System.out.println(montString);
	}
}