public class Tipedata{
	public static void main(String args[]){
		/* change this line
		belajar variable
		variable itu diibaratkan seperti kotak/box yang bisa diisi oleh sesuatu,variable ini mengandung 2 sesuatu :
		1. nama
		2. value
		nama ini biasanya jarang berubah, namun kalau value ini sering berubah-ubah. value bisa berisi angka atau huruf, 
		ini tergantung dari tipe data dari nama (kita bisa sebut nama ini adalah tipe data eh gk juga sih).
		misal kalau nama adalah int maka value akan berisi angka bulat begitu juga jika nama adalah String maka value akan berisi karakter.
		
		contoh sebuah variable*/

		int panjang; //ini namanya deklarasi variable, ngasih nama tapi tidak ngasih value, nilainya belum ada.
		int lebar; // tambah variable lebar dengan tipe data int
		int luas; // tambah variable luas dengan tipe data int
		/*
		nama variable adalah panjang, tipenya int berarti valuenya angka.
		 */
		panjang = 8; // ini namanya inisialize/ inisialisasi yaitu penambahan nilai pada variable
		lebar = 4; // inisialisasi lebar
		luas = panjang * lebar; // inisialisasi luas dengan value panjang * lebar
		// update nilai nilai
		// panjang 1 (nilai awal 8) ----------------- panjang 2 (+2)----------- panjang 3(-4) ---- nilai panjang akhir = 6
		panjang = panjang + 2; // panjang 2
		panjang = panjang - 4; // panjang 3
		System.out.println("Panjang = "+ panjang +" Lebar = "+ lebar); // mencetak nilai panjang akhir
		System.out.println("Luas Persegi panjang = "+luas); // mencetak hasil luas yang mana valuenya merupakan hasil perkalian var panjang dan lebar

		/*Hari kedua kita akan belajar tentang String data type
		String dijava berarti karakter yang bisa berisi symbol angka dan alfabet*/
		
		// deklarasi variable
		String firstName; 
		String lastName;
		String fullName;
		int length;
		
		//inisialize atau kalau di String biasa menggunakan String literal (pake "")
		firstName = "Jack"; 
		lastName = "Butland";

		firstName = firstName.toUpperCase(); // function untuk mengubah value string menjadi huruf besar semua
		length = firstName.length(); // function untuk mengetahui panjang karakter dari value suatu string (name)
		System.out.println(firstName); //mencetak value string
		System.out.println(length);
		// String concatenation 
		// ini merupakan penggabungan 2 String atau lebih, gk cuman string sih kita bisa sebut penggabungan variable kali ya
		// sebenarnya kita pernah mencobanya yaitu pas kita menambilkan panjang lebar di pembahasan int
		// mari kita coba kalau di String seperti apa
		fullName = firstName + " " +lastName;
		System.out.println(fullName);
		/*tentang penamaan nama variable
			dalam java penamaan variable dalam java menggunakan lowerCamelCase sedangkan untuk UpperCamelCase digunakan
			untuk penamaan Class. Terdapat tiga aturan penamaan variable
			1. nama variable dalam java adalah case sensitive, meskipun karakter sama namun beda besar kecilnya huruf maka akan dianggap beda
			2. diawali dengan menggunakan huruf tidak boleh dengan angka
			3. tidak boleh menggunakan spasi.
			kita coba ya
		*/
		//deklarasi dan inisialize variable
		int umur = 12;
		String namaBapak = "Heru";
		int jmlAnak = 6;
		namaBapak = namaBapak.toUpperCase();
		System.out.println(umur);
		System.out.println(namaBapak);
		System.out.println(jmlAnak);
		/*
		membahas tentang data type
		variable data type (number)
		1. integer -> untuk bilangan bulat kapasitasnya sampai 2147483647
		2. long -> untuk bilangan bulat namun memiliki kapasitas yang lebih besar dar integer ykni 2147483647*10000000
		3. double -> untuk bilangan pecahan kapasitasnya sampai 99.275

		variable data type (text)
		String -> untuk full karakter atau untuk teks panjang penulisanya menggunakan "" atau String literal
		char -> untuk 1 karakter penulisannya menggunakan ''

		variable data tyoe (decision)
		boolean -> digunakan untuk menampilkan true or false */
		char kelas = 'A';
		long jumlahSiswa = 35000000;
		long jumlahGuru = 100000;
		long perkalianOrang = jumlahSiswa * jumlahGuru;
		System.out.println(kelas);
		System.out.println(perkalianOrang);

		/*kita belajar tentang variable aritmatika
		terdapat beberapa operasi yang sering digunakan
		1. perkalian
		2. pembagian
		3. penjumlahan 
		4. pengurangan

		untuk pembagian itu, bilangan pembagi dianjurkan untuk bertipe data double knp? karena biar hasil pembagiannya
		bisa akurat, kalau int bisa2 aja namun nanti hasilnya akan berbeda, contoh */
		double jmlNilai = 24/5; //dijava disebut dengan truncation
		double nilai = 24/5.0;
		System.out.println(jmlNilai); //maka hasilnya adalah 4.0 bukan 4.8 karena tipe data pembaginya adalh int
		System.out.println(nilai); //outputnya adalah 4.8

		/*perlu menambahkan () untuk operasi yang ingin didahulukan karena java akan mengeksekusi terlebih dahulu
		1. perkalian dan pembagian
		2. penambahan dan pengurangan, contoh*/
		double paid = 10;
		double change = 4.45;
		double tip = (paid - change) * 0.2;
		double tip2 = paid - change * 0.2;
		System.out.println(tip); // output 1.11
		System.out.println(tip2);// output 9.11
		/*casting dalam java
		casting = mengubah / mengkonversi ke dalam tipe data lain misal*/
		int gajiPokok = 24;
		int potongan = 5;
		double totalGaji = (double) gajiPokok/potongan; //ini yang dinamakan casting, knp? krn jika tidak pake casting mk hasilnya adl 4.0 bkn 4.8
		System.out.println(totalGaji);

		double a = 17;
		double b = 45.33;
		double c = a * b;
		System.out.println(c); //output = 770.61
		int d = (int) c; // this is casting in java
		System.out.println(d); // output = 770

		// membuat program average nilai
		// deklarasi dan inisialize variable
		double math = 97.5;
		double english = 98;
		double science = 83.5;
		double drama = 75;
		double music = 96;
		double sum = math + english + science + drama + music;
		double average = sum / 5;
		// mencetak hasil average
		System.out.println(average);
		}
	}
}
